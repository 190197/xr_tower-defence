using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager> {

    GameObject gameObjectToBuild;
    private void OnEnable () {
        ImageTracking.OnUpdateTrackedGO.AddListener (CurrentTrackedGameObject);
    }
    private void OnDisable () {
        ImageTracking.OnUpdateTrackedGO.RemoveListener (CurrentTrackedGameObject);
    }
    public void BuildTrackedImage () {
        Debug.Log("Construction Button Pressed");

        if (gameObjectToBuild != null) {
            Debug.LogWarning ("gameObjectToBuild is not there");

            ConstructionManager.Instance.BuildTrackedImage (gameObjectToBuild);
        }
    }

    private void CurrentTrackedGameObject (GameObject currentGO) {
        gameObjectToBuild = currentGO;
        Debug.Log ($"Current stored GameObjectToBuild {gameObjectToBuild.name}");
    }

}