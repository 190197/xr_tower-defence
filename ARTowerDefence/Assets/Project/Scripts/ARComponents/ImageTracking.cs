using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;

[RequireComponent (typeof (ARTrackedImageManager))] // Referencing this class 
public class ImageTracking : Singleton<ImageTracking> {

    [Header ("Placeable Items")]
    [SerializeField] GameObject[] placeablePrefabs; // Create them at runtime

    Dictionary<string, GameObject> spawnedPrefabs = new Dictionary<string, GameObject> ();
    ARTrackedImageManager trackedImageManager;

    /* Unity Events */
    #region Unity Events

    public static UnityEvent<GameObject> OnUpdateTrackedGO = new UnityEvent<GameObject> (); // Aways make sure tracked Image and GO has the same name.

    #endregion

    private void Awake () {
        trackedImageManager = FindObjectOfType<ARTrackedImageManager> ();

        // Prespawn the objects 
        foreach (GameObject prefab in placeablePrefabs) {
            GameObject newPrefab = Instantiate (prefab, Vector3.zero, Quaternion.identity);
            newPrefab.name = prefab.name; // Assigning name for comparing later
            spawnedPrefabs.Add (prefab.name, newPrefab);
            newPrefab.SetActive (false);
        }
    }

    private void OnEnable () {
        trackedImageManager.trackedImagesChanged += ImageChanged;
    }

    private void OnDisable () {
        trackedImageManager.trackedImagesChanged -= ImageChanged;
    }

    // Allows to call the events added updated and removed
    private void ImageChanged (ARTrackedImagesChangedEventArgs eventArgs) {
        foreach (ARTrackedImage trackedImage in eventArgs.added) {
            UpdateImage (trackedImage);
        }

        foreach (ARTrackedImage trackedImage in eventArgs.updated) {
            UpdateImage (trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.removed) {
            spawnedPrefabs[trackedImage.name].SetActive (false);
        }
    }

    private void UpdateImage (ARTrackedImage trackedImage) {
        string name = trackedImage.referenceImage.name;
        Vector3 position = trackedImage.transform.position;
        Quaternion rotation = trackedImage.transform.rotation;

        GameObject prefab = spawnedPrefabs[name];
        prefab.transform.position = position;
        prefab.transform.rotation = rotation;

        prefab.SetActive (true);

        foreach (GameObject go in spawnedPrefabs.Values) {
            if (go.name != name) {
                go.SetActive (false);
            } else if (go.name == name && go != null) {
                OnUpdateTrackedGO.Invoke (prefab);
                Debug.Log ("Event: Assigning building Prefab");
            }
        }

    }

}