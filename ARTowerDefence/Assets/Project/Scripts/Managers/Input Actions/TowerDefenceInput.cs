// GENERATED AUTOMATICALLY FROM 'Assets/Project/Scripts/Managers/Input Actions/TowerDefenceInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @TowerDefenceInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @TowerDefenceInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""TowerDefenceInput"",
    ""maps"": [
        {
            ""name"": ""Interactions"",
            ""id"": ""23e711fc-2ec8-406a-b27a-e138819eb1a7"",
            ""actions"": [
                {
                    ""name"": ""Touch"",
                    ""type"": ""Button"",
                    ""id"": ""3e1fe99c-2447-4b3e-bea2-d8e7ccc2ae33"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""86b8142a-b0b8-460a-98af-4715c8f476a6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7054ccd3-4d5a-4b23-b9c9-3d174204c7a7"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Touch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""16a8394e-3a13-4643-bea7-1f99f2e5fd43"",
                    ""path"": ""<Touchscreen>/primaryTouch/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Interactions
        m_Interactions = asset.FindActionMap("Interactions", throwIfNotFound: true);
        m_Interactions_Touch = m_Interactions.FindAction("Touch", throwIfNotFound: true);
        m_Interactions_MousePosition = m_Interactions.FindAction("MousePosition", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Interactions
    private readonly InputActionMap m_Interactions;
    private IInteractionsActions m_InteractionsActionsCallbackInterface;
    private readonly InputAction m_Interactions_Touch;
    private readonly InputAction m_Interactions_MousePosition;
    public struct InteractionsActions
    {
        private @TowerDefenceInput m_Wrapper;
        public InteractionsActions(@TowerDefenceInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Touch => m_Wrapper.m_Interactions_Touch;
        public InputAction @MousePosition => m_Wrapper.m_Interactions_MousePosition;
        public InputActionMap Get() { return m_Wrapper.m_Interactions; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InteractionsActions set) { return set.Get(); }
        public void SetCallbacks(IInteractionsActions instance)
        {
            if (m_Wrapper.m_InteractionsActionsCallbackInterface != null)
            {
                @Touch.started -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnTouch;
                @Touch.performed -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnTouch;
                @Touch.canceled -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnTouch;
                @MousePosition.started -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnMousePosition;
            }
            m_Wrapper.m_InteractionsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Touch.started += instance.OnTouch;
                @Touch.performed += instance.OnTouch;
                @Touch.canceled += instance.OnTouch;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
            }
        }
    }
    public InteractionsActions @Interactions => new InteractionsActions(this);
    public interface IInteractionsActions
    {
        void OnTouch(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
    }
}
