using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

//Enum to check type of building

public class ConstructionManager : Singleton<ConstructionManager> {

    [Header ("To Keep tracked of Build Objects")]
    [SerializeField] List<GameObject> buildingBuild = new List<GameObject> ();

    [Header ("Settings")]

    [SerializeField] LayerMask buildingMask;

    [Header ("Testing")]
    [SerializeField] Material buildingBuiltMaterial;

    /* 
        Determins the building location for the objects
    */
    #region Building Zone 

    int buildCount = 0; // A check to see if this is the first Building or not - this will be used to build the next object on the same y axis.
    float gridYLocation;

    #endregion

    // Unit of measurement 0.9
    /* 
        Gets TrackedImageGameObject and Instantiat it 
    */
    public void BuildTrackedImage (GameObject trackedGOToConstruct) {

        GameObject currentGO = trackedGOToConstruct;

        if (currentGO != null) {
            if (buildCount == 0) {
                Vector3 newBuildLocation = new Vector3 (currentGO.transform.position.x, currentGO.transform.position.y, currentGO.transform.position.z);
                ConstructBuilding (currentGO, newBuildLocation);
            } else {

                Vector3 snappedBuildLocation = Vector3.zero;
                for (int i = 0; i < buildingBuild.Count; i++) {
                    GameObject previousGameObject = buildingBuild[buildCount - 1];
                    Debug.LogError ($"Current tracked location {currentGO.transform.position}");
                    snappedBuildLocation = SnapBuildingToGrid (previousGameObject, currentGO);
                    Debug.LogError ($"Previous snapped build location {previousGameObject.transform.position}");
                }
                Debug.LogError ($"new snapped build location {snappedBuildLocation}");
                ConstructBuilding (currentGO, snappedBuildLocation);
            }

        }
    }

    //! Add rotation later
    private void ConstructBuilding (GameObject currentGO, Vector3 buildLocation) {
        GameObject newlyBuildObject = Instantiate (currentGO, buildLocation, currentGO.transform.rotation);
        newlyBuildObject.GetComponentInChildren<Renderer> ().material = buildingBuiltMaterial;
        newlyBuildObject.layer = buildingMask;
        buildingBuild.Add (newlyBuildObject);
        Debug.LogWarning ($"Building Constructed, Layer Mask is {newlyBuildObject.layer}");

        gridYLocation = newlyBuildObject.transform.position.y;

        buildCount++;
    }

    /* 
         Transform for last build object 
         valid positions Vector 2 will be N E S W
    */

    private Vector3 SnapBuildingToGrid (GameObject previousGameObject, GameObject currentTrackedGO) {
        Vector3 newBuildLocation = Vector3.zero;
        if (previousGameObject == null) {
            Debug.Log ("GameObject is null");
            return Vector3.zero;
        } else {
            Transform currentTransform = currentTrackedGO.transform;
            float x = currentTransform.position.x;
            float z = currentTransform.position.z;

            if (x < 0 || z < 0) {
                x = x - 1;
                z = z - 1;

                if (x > z) {
                    newBuildLocation.x = (previousGameObject.transform.position.x + 0.09f) * -1;
                    newBuildLocation.z = previousGameObject.transform.position.z;
                    newBuildLocation.y = gridYLocation;
                } else {
                    newBuildLocation.z = (previousGameObject.transform.position.z + 0.09f) * -1;
                    newBuildLocation.x = previousGameObject.transform.position.x;
                    newBuildLocation.y = gridYLocation;
                }

                return newBuildLocation;

            } else {
                if (x > z) {
                    newBuildLocation.x = previousGameObject.transform.position.x + 0.09f;
                    newBuildLocation.z = previousGameObject.transform.position.z;
                    newBuildLocation.y = gridYLocation;
                } else {
                    newBuildLocation.z = previousGameObject.transform.position.z + 0.09f;
                    newBuildLocation.x = previousGameObject.transform.position.x;
                    newBuildLocation.y = gridYLocation;
                }
                return newBuildLocation;
            }

        }

    }

}