using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour, IBuilding {

    [Header ("Settings")]
    [SerializeField] LayerMask collideMask;

    Collider[] colliders;
    public GameObject thisGameObject;
    //? Make colliders array 
    //? Update at runtime
    //? Get the current gameobject you want to check collisions with
    //? Delete colliding objects/objects
    private void OnEnable () {
        colliders = Physics.OverlapBox (transform.position, (Vector3.one * 0.09f) / 2, Quaternion.identity, collideMask, QueryTriggerInteraction.Collide);

    }
    //! Might need to do this at runtime rather than in a function call;
    public void CheckCollision () {
        for (int i = 0; i < colliders.Length; i++) {
            if (colliders[i].gameObject.TryGetComponent<IBuilding> (out IBuilding building)) {
                Debug.LogWarning (colliders[i].gameObject.name);
                building.DestroyGameObject (); //! Only destroy on function call
            }
        }
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube (transform.position, Vector3.one * 0.09f);
    }

    public void DestroyGameObject () {
        Destroy (this.gameObject);
    }

}