using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class InputManager : Singleton<InputManager> {

    TowerDefenceInput ARTowerDefenceInput;
    static Vector2 mousePosition;

    /* 
        Unity Events
    */
    public static UnityEvent<Vector2> OnInteract = new UnityEvent<Vector2> ();

    private void Start () {
        ARTowerDefenceInput = new TowerDefenceInput ();
        ARTowerDefenceInput.Enable ();

        ARTowerDefenceInput.Interactions.MousePosition.performed += x => {
            mousePosition = x.ReadValue<Vector2> ();
        };

        ARTowerDefenceInput.Interactions.Touch.performed += x => {
            if (!HitUI ()) {
                OnInteract.Invoke (mousePosition);
            }
        };
    }

    private void OnDestroy () {
        ARTowerDefenceInput.Dispose ();
    }
    public static bool HitUI () { // Check if its hitting UI and sends a trie or false.
        PointerEventData pointer = new PointerEventData (EventSystem.current);
        pointer.position = mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult> ();
        EventSystem.current.RaycastAll (pointer, raycastResults);

        if (raycastResults.Count > 0) {
            // Debug.Log($"Hit UI: {raycastResults[0].gameObject.name}");

            return true;
        }

        return false;
    }

}